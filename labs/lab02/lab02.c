#define WOKWI               // Uncomment if running on Wokwi RP2040 emulator.
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"  // Comment if running on Wokwi RP2040 emulator.
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.

// Wallis algorithm function (single-precision)
float wallis_single(int n) {                  // Takes iterations as argument
  float pi_approx = 2.0f;                     // Initialise Pi with first term of Wallis product (2)

  for (int i = 1; i <= n; i++) {              // Product of Sum
    float numerator = 4.0f * i * i;           // 4n^2
    float denominator = 4.0f * i * i - 1.0f;  // 4n^2 - 1
    pi_approx *= numerator / denominator;     // 1st itrtn.:(next approx) = [curr approx (2)]*(numer/denom))
  }

  return pi_approx;
}
// Wallis algorithm function (double-precision)
double wallis_double(int n) {                 // Takes itterations as argument
  double pi_approx = 2.0;                     // Initialise Pi with first term of Wallis product (2)

  for (int i = 1; i <= n; i++) {              // Product of Sum
    double numerator = 4.0 * i * i;           // 4n^2
    double denominator = 4.0 * i * i - 1.0;   // 4n^2 - 1
    pi_approx *= numerator / denominator;     // 1st itrtn.:(next approx) = [curr approx (2)]*(numer/denom))
  }

  return pi_approx;
}

//--------------------------------------------------------------

int main() {

#ifndef WOKWI
  // Initialise the IO as we will be using the UART
  // Only required for hardware and not needed for Wokwi
  stdio_init_all();
#endif


  int iterations = 100000;        // Default # of iterations (More iterations = more accuracy)

//-----------
    printf("Calculating PI using single-precision...\n");
    float calculated_pi_single = wallis_single(iterations);
    float default_pi_single = 3.14159265359f;               // Via blackboard || level of precision (.11)

    printf("Calculated PI (Single Precision)        : %.11f\n", calculated_pi_single);
    printf("Default PI                              : %.11f\n", default_pi_single);

    float approx_error_single = fabs(default_pi_single - calculated_pi_single);            // fabs() used to derive magnitude of approximation error
    printf("Approximation Error (Single Precision)  : %.11f\n\n", approx_error_single);

//-----------
    printf("Calculating PI using double-precision...\n");
    double calculated_pi_double = wallis_double(iterations);
    double default_pi_double = 3.14159265359;               // Via blackboard || level of precision (.11)

    printf("Calculated PI (Double Precision)        : %.11lf\n", calculated_pi_double);
    printf("Default PI                              : %.11lf\n", default_pi_double);

    double approx_error_double = fabs(default_pi_double - calculated_pi_double);    // fabs() used to derive magnitude of approximation error
    printf("Approximation Error (Double Precision)  : %.11lf\n", approx_error_double);

//-----------------------------------
    return 0;
}
