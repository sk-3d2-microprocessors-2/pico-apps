#include "pico/stdlib.h"
// Function to make microcontrollers LED blink
void blink(lpin, ldelay) {
    while (true) {
        // Toggle LED on then sleep for delay period
        gpio_put(lpin, 1);
        sleep_ms(ldelay);

        // Toggle LED off then sleep for delay period
        gpio_put(lpin, 0);
        sleep_ms(ldelay);
    }
}

int main(){
    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    // infinite while loop
    blink(LED_PIN, LED_DELAY);
      
    // Should never get here due to infinite while-loop.
    return 0;
}